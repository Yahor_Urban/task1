package com.epam.urban.newsmanagement.constant;

public final class Constants {

	public static final String POOLSIZE = "connection.poolsize";
	public static final String DRIVER = "connection.driver";
	public static final String URL = "connection.url";
	public static final String USER = "connection.user";
	public static final String PASSWORD = "connection.password";
	
	public static final String ADD_NEWS = "dao.news.add_news";
	public static final String EDIT_NEWS = "dao.news.edit_news";
	public static final String DEL_NEWS = "dao.news.del_news";
	
	public static final String LIST_NEWS = "dao.news.list_news";
	public static final String GET_NEWS_BY_ID = "dao.news.get_news_by_id";
	public static final String GET_NEWS_BY_AUTHOR = "dao.news.get_news_by_author";
	public static final String GET_NEWS_BY_TAG = "dao.news.get_news_by_tag"; 
	
	public static final String ADD_AUTHOR = "dao.author.add_author";
	public static final String ADD_NEWS_AUTHOR = "dao.author.add_news_author";
	public static final String GET_AUTHOR_LIST = "dao.author.get_author_list";
	public static final String DEL_AUTHOR = "dao.author.del_author";
	public static final String DEL_AUTHOR_BY_ID = "dao.author.del_author_by_id";
	public static final String DEL_NEWS_AUTHOR = "dao.author.del_news_author";
	public static final String EDIT_AUTHOR = "dao.author.edit_author";
	
	public static final String ADD_COMMENT = "dao.comment.add_comment";
	public static final String DEL_COMMENT = "dao.comment.del_comment";
	public static final String GET_COUNT = "dao.comment.get_count"; 
	public static final String DEL_COMMENTS = "dao.comment.del_comments";
	public static final String EDIT_COMMENT = "dao.comment.edit_comment";
	
	public static final String ADD_TAG = "dao.tag.add_tag";
	public static final String GET_TAG_LIST = "dao.tag.get_tag_list";
	public static final String ADD_NEWS_TAG = "dao.tag.add_news_tag";
	public static final String DEL_NEWS_TAG = "dao.tag.del_news_tag";

	public static final String NEWS_ID = "news_id";
	public static final String SHORT_TEXT = "short_text";
	public static final String FULL_TEXT = "full_text";
	public static final String TITLE = "title";
	public static final String CREATION_DATE = "creation_date";
	public static final String MODIFICATION_DATE = "modification_date";
	public static final String AUTHOR_ID = "author_id";
	public static final String NAME = "name";
	public static final String TAG_NAME = "tag_name";
	public static final String TAG_ID = "tag_id";
	
	public static final String RESOURCES_PATH="database";
	
	private Constants() {
	}
}
