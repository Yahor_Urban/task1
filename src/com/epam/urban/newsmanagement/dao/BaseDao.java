package com.epam.urban.newsmanagement.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import com.epam.urban.newsmanagement.dao.pool.IConnectionPool;
import com.epam.urban.newsmanagement.resource.DatabaseResources;
import com.epam.urban.newsmanagement.resource.IResources;

/**
 * Abstract dao class
 * @author Urban Egor
 *
 */
public abstract class BaseDao {

	private static final Logger LOG = Logger.getLogger(BaseDao.class);

	protected IConnectionPool connectionPool;
	protected IResources resources;

	public BaseDao(IConnectionPool connectionPool) {
		this.resources = DatabaseResources.getInstance();
		this.connectionPool = connectionPool;
	}

	protected PreparedStatement openPrepStatement(Connection connection, String sqlPattern) throws SQLException {
		PreparedStatement statement = null;
		String sql = resources.getString(sqlPattern);
		statement = connection.prepareStatement(sql);
		return statement;
	}

	protected CallableStatement openCallStatement(Connection connection, String sqlPattern) throws SQLException {
		CallableStatement statement = null;
		String sql = resources.getString(sqlPattern);
		statement = connection.prepareCall(sql);
		return statement;
	}

	protected void closeStatement(Statement statement) {
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				LOG.error("Statement closing failed", e);
			}
		}
	}
}
