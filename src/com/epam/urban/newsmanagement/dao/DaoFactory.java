package com.epam.urban.newsmanagement.dao;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;

import com.epam.urban.newsmanagement.dao.pool.ConnectionPool;
import com.epam.urban.newsmanagement.dao.pool.IConnectionPool;
import com.epam.urban.newsmanagement.exception.DAOException;

/**
 * Initializes dao layer
 * @author Egor Urban
 *
 */
public class DaoFactory implements IDaoFactory {

	private static final Logger LOG = Logger.getLogger(DaoFactory.class);
	private static final Lock LOCK = new ReentrantLock();
	private static volatile DaoFactory instance;
	private static AtomicBoolean isInitialized = new AtomicBoolean();
	private static AtomicBoolean isDisposed = new AtomicBoolean();
	private IConnectionPool connectionPool;
	
	private NewsDao newsDao;
	private AuthorDao authorDao;
	private CommentDao commentDao;
	private TagDao tagDao;

	private DaoFactory() throws DAOException {
		init();
	}

	/**
	 * Initializes dao factory
	 * @return Dao factory instance
	 * @throws DAOException
	 */
	public static IDaoFactory getInstance() throws DAOException {
		if (instance == null && !isInitialized.get()) {
			try {
				LOCK.lock();
				if (instance == null) {
					instance = new DaoFactory();
					isInitialized.set(true);
					LOG.info("Dao factory has been initialized");
				}
			} finally {
				LOCK.unlock();
			}
		}
		return instance;
	}

	public NewsDao getNewsDao() {
		return this.newsDao;
	}

	public AuthorDao getAuthorDao() {
		return this.authorDao;
	}
	
	public CommentDao getCommentDao() {
		return this.commentDao;
	}
	
	public TagDao getTagDao() {
		return this.tagDao;
	}
	
	private void init() throws DAOException {
		this.connectionPool = ConnectionPool.getInstance();

		initNewsDao();
		initAuthorDao();
		initCommentDao();
		initTagDao();
	}

	private void initNewsDao() {
		this.newsDao = new NewsDao(connectionPool);
	}

	private void initAuthorDao() {
		this.authorDao = new AuthorDao(connectionPool);
	}
	
	private void initCommentDao() {
		this.commentDao = new CommentDao(connectionPool);
	}
	
	private void initTagDao() {
		this.tagDao = new TagDao(connectionPool);
	}
	
	@Override
	public void dispose() {
		if (instance != null && !isDisposed.get()) {
			try {
				LOCK.lock();
				if (instance != null) {
					this.connectionPool.dispose();
					isDisposed.set(true);
					instance = null;
					LOG.info("Dao factory was disposed");
				}
			} finally {
				LOCK.unlock();
			}
		}
	}
}
