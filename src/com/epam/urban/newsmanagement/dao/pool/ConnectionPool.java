package com.epam.urban.newsmanagement.dao.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;

import com.epam.urban.newsmanagement.constant.Constants;
import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.resource.DatabaseResources;
import com.epam.urban.newsmanagement.resource.IResources;


/**
 * Connection pool class
 * @author Urban Egor
 *
 */
public class ConnectionPool implements IConnectionPool {

	private static final Logger LOG = Logger.getLogger(ConnectionPool.class);
	private static final Lock LOCK = new ReentrantLock();
	private static ConnectionPool instance;
	private static AtomicBoolean isInitialized = new AtomicBoolean();
	private static AtomicBoolean isDisposed = new AtomicBoolean();

	private BlockingQueue<Connection> connectionQueue;
	private IResources resources;

	private ConnectionPool() throws DAOException {
		resources = DatabaseResources.getInstance();
		String poolSize = resources.getString(Constants.POOLSIZE);
		String driverName = resources.getString(Constants.DRIVER);
		String url = resources.getString(Constants.URL);
		String user = resources.getString(Constants.USER);
		String password = resources.getString(Constants.PASSWORD);
		int size = Integer.parseInt(poolSize);
		connectionQueue = new ArrayBlockingQueue<Connection>(size);
		registerDriver(driverName);
		fillConnectionPool(size, url, user, password);
		LOG.info("Connection pool was initialized");
	}

	/**
	 * Initializes connection pool
	 * @return Connection pool instance
	 * @throws DAOException
	 */
	public static IConnectionPool getInstance() throws DAOException {
		if (instance == null && !isInitialized.get()) {
			try {
				LOCK.lock();
				if (instance == null) {
					instance = new ConnectionPool();
					isInitialized.set(true);
					LOG.info("Connection pool has been initialized");
				}
			} finally {
				LOCK.unlock();
			}
		}
		return instance;
	}

	
	@Override
	public Connection takeConnection() throws DAOException {
		try {
			return connectionQueue.take();
		} catch (InterruptedException e) {
			throw new DAOException("Taking connection failed", e);
		}
	}

	
	@Override
	public void releaseConnection(Connection connection) throws DAOException {
		if (connection == null) {
			throw new DAOException("Connection is null");
		}
		try {
			if (!connection.isClosed()) {
				if (!connectionQueue.offer(connection)) {
					throw new DAOException("Connection pool is full");
				}
			} else {
				throw new DAOException("Connection is closed");
			}
		} catch (SQLException e) {
			throw new DAOException("Release connection failed", e);
		}

	}

	
	@Override
	public void dispose() {
		if (instance != null && !isDisposed.get()) {
			try {
				LOCK.lock();
				isDisposed.set(true);
				Connection connection;
				while ((connection = connectionQueue.poll()) != null) {
					try {
						connection.close();
					} catch (SQLException e) {
						LOG.error("Connection cloasing failed", e);
					}
				}
				instance = null;
				isInitialized.set(false);
				LOG.info("Connection pool was disposed");
			} finally {
				LOCK.unlock();
			}
		}
	}

	private void registerDriver(String driverName) throws DAOException {
		try {
			Class.forName(driverName);
		} catch (ClassNotFoundException e) {
			throw new DAOException("Driver creating failed", e);
		}
	}

	private void fillConnectionPool(int size, String url, String user, String password) throws DAOException {
		try {
			for (int i = 0; i < size; i++) {
				Connection connection = DriverManager.getConnection(url, user, password);
				connectionQueue.offer(connection);
			}
		} catch (SQLException e) {
			throw new DAOException("Connection creating failed", e);
		}
	}

}
