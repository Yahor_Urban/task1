package com.epam.urban.newsmanagement.dao.pool;

import java.sql.Connection;

import com.epam.urban.newsmanagement.exception.DAOException;

/**
 * Connection pool interface
 * @author Urban Egor
 *
 */
public interface IConnectionPool {

	/**
	 * Takes db connection
	 * @return db connection
	 * @throws DAOException
	 */
	public Connection takeConnection() throws DAOException;
	
	/**
	 * Release connection
	 * @throws DAOException
	 */
	public void releaseConnection(Connection connection) throws DAOException;
	
	/**
	 * Disposes connection pool
	 */
	public void dispose();

}
