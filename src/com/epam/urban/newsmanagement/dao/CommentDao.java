package com.epam.urban.newsmanagement.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.epam.urban.newsmanagement.constant.Constants;
import com.epam.urban.newsmanagement.dao.pool.IConnectionPool;
import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.model.Comment;

/**
 * Comment dao class
 * @author Urban Egor
 *
 */
public class CommentDao extends BaseDao {

	private static final Logger LOG = Logger.getLogger(CommentDao.class);
	
	public CommentDao(IConnectionPool connectionPool) {
		super(connectionPool);
		LOG.info("Comment dao has been initialized");
	}

	/**
	 * Add comment
	 * @param comment
	 * @return {@code true}, if comment successfully added
	 * @throws DAOException
	 */
	public boolean addComment(Comment comment) throws DAOException {
		boolean flag = false;
		Connection connection = connectionPool.takeConnection();
		PreparedStatement statement = null;

		try {
			statement = openPrepStatement(connection, Constants.ADD_COMMENT);
			statement.setString(1, comment.getCommentText());
			statement.setString(2, comment.getCreationDate().toString());
			statement.setLong(3, comment.getNewsId());
			statement.executeUpdate();
			flag = true;
		} catch (SQLException e) {
			throw new DAOException("Adding comment failed", e);
		} finally {
			closeStatement(statement);
			connectionPool.releaseConnection(connection);
		}

		return flag;
	}
	
	/**
	 * Delete comment by id
	 * @param comment id
	 * @return {@code true}, if comment successfully deleted
	 * @throws DAOException
	 */
	public boolean deleteComment(Long commentId) throws DAOException {
		boolean flag = false;
		Connection connection = connectionPool.takeConnection();
		PreparedStatement statement = null;

		try {
			statement = openPrepStatement(connection, Constants.DEL_COMMENT);
			statement.setLong(1, commentId);
			statement.executeUpdate();
			flag = true;
		} catch (SQLException e) {
			throw new DAOException("Delleting comment failed", e);
		} finally {
			closeStatement(statement);
			connectionPool.releaseConnection(connection);
		}

		return flag;
	}
	
	public boolean editComment(Comment comment) throws DAOException {
		boolean flag = false;
		Connection connection = connectionPool.takeConnection();
		PreparedStatement statement = null;
		
		try {
			statement = openPrepStatement(connection, Constants.EDIT_COMMENT);
			statement.setLong(2, comment.getId());
			statement.setString(1, comment.getCommentText());
			statement.executeUpdate();
			flag = true;
		} catch (SQLException e) {
			throw new DAOException("Edit comment failed", e);
		} finally {
			closeStatement(statement);
			connectionPool.releaseConnection(connection);
		}
		
		return flag;
	}
	
}
