package com.epam.urban.newsmanagement.dao;

/**
 * Dao factory interface
 * @author Egor Urban
 *
 */
public interface IDaoFactory {

	/**
	 * @return {@code NewsDao} instance
	 */
	public NewsDao getNewsDao();
	
	/**
	 * @return {@code AuthorDao} instance
	 */
	public AuthorDao getAuthorDao();
	
	/**
	 * @return {@code CommentDao} instance
	 */
	public CommentDao getCommentDao();
	
	/**
	 * @return {@code TagDao} instance
	 */
	public TagDao getTagDao();
	
	/**
	 * Disposes dao factory
	 */
	public void dispose();

}
