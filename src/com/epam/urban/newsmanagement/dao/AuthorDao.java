package com.epam.urban.newsmanagement.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.epam.urban.newsmanagement.constant.Constants;
import com.epam.urban.newsmanagement.dao.pool.IConnectionPool;
import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.model.Author;

/**
 * Author dao class
 * @author Urban Egor
 *
 */
public class AuthorDao extends BaseDao {

	private static final Logger LOG = Logger.getLogger(AuthorDao.class);
		
	public AuthorDao(IConnectionPool connectionPool) {
		super(connectionPool);
		LOG.info("Author dao has been initialized");
	}
	
	/**
	 * Add single author
	 * @param author name
	 * @return author id
	 * @throws DAOException
	 */
	public Long addAuthor(String name) throws DAOException {
		Long id = (long) -1;
		Connection connection = connectionPool.takeConnection();
		PreparedStatement statement = null;

		try {
			statement = openPrepStatement(connection, Constants.ADD_AUTHOR);
			statement.setString(1, name);
			statement.executeUpdate();
			id = getAuthorId(name);
		} catch (SQLException e) {
			throw new DAOException("Adding author failed", e);
		} finally {
			closeStatement(statement);
			connectionPool.releaseConnection(connection);
		}

		return id;
	}
	
	/**
	 * Add author by news id
	 * @param news id
	 * @param author id
	 * @return {@code true}, if author successfully added
	 * @throws DAOException
	 */
	public boolean addAuthor(Long newsId, Long authorId) throws DAOException {
		boolean flag = false;
		Connection connection = connectionPool.takeConnection();
		PreparedStatement statement = null;

		try {
			statement = openPrepStatement(connection, Constants.ADD_NEWS_AUTHOR);
			statement.setLong(1, newsId);
			statement.setLong(2, authorId);
			statement.executeUpdate();
			flag = true;
		} catch (SQLException e) {
			throw new DAOException("Adding author failed", e);
		} finally {
			closeStatement(statement);
			connectionPool.releaseConnection(connection);
		}

		return flag;
	}
	
	/**
	 * Edit author by id
	 * @param author
	 * @return {@code true}, if successfully changed
	 * @throws DAOException
	 */
	public boolean editAuthor(Author author) throws DAOException {
		if (author == null) {
			throw new DAOException("Author is null");
		}
		boolean flag = false;
		
		Connection connection = connectionPool.takeConnection();
		PreparedStatement statement = null;
		
		try {
			statement = openPrepStatement(connection, Constants.EDIT_AUTHOR);
			statement.setLong(2, author.getAuthorId());
			statement.setString(1, author.getName());
			statement.executeUpdate();
			flag = true;
		} catch (SQLException e) {
			throw new DAOException("Edit author failed", e);
		} finally {
			closeStatement(statement);
			connectionPool.releaseConnection(connection);
		}
		
		return flag;
	}
	
	/**
	 * Delete author by author id
	 * @param id
	 * @return {@code true}, if successfully deleted
	 * @throws DAOException
	 */
	public boolean deleteAuthor(Long id) throws DAOException {
		if (id == null) {
			throw new DAOException("Author id is null");
		}
		boolean flag = false;
		
		Connection connection = connectionPool.takeConnection();
		PreparedStatement statement = null;
		PreparedStatement statement1 = null;
		
		try {
			connection.setAutoCommit(false);
			statement = openPrepStatement(connection, Constants.DEL_AUTHOR);
			statement1 = openPrepStatement(connection, Constants.DEL_AUTHOR_BY_ID);
			statement.setLong(1, id);
			statement1.setLong(1, id);
			statement.executeUpdate();
			statement1.executeUpdate();
			connection.commit();
			flag = true;
		} catch (SQLException e) {
			try {
				connection.rollback();
				throw new DAOException("Delleting author failed", e);
			} catch (SQLException e1) {
				throw new DAOException("Rollback failed", e1);
			}
		} finally {
			closeStatement(statement);
			closeStatement(statement1);
			connectionPool.releaseConnection(connection);
		}
		
		return flag;
	}
	
	/**
	 * To getting author id by name
	 * @param author name
	 * @return author id
	 * @throws DAOException
	 */
	public Long getAuthorId(String authorName) throws DAOException {
		Connection connection = connectionPool.takeConnection();
		PreparedStatement statement = null;

		try {
			statement = openPrepStatement(connection, Constants.GET_AUTHOR_LIST);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				String name = resultSet.getString(Constants.NAME);
				if (authorName.equalsIgnoreCase(name)) {
					return resultSet.getLong(Constants.AUTHOR_ID);
				}
			}
		} catch (SQLException e) {
			throw new DAOException("Getting autor id failed", e);
		} finally {
			closeStatement(statement);
			connectionPool.releaseConnection(connection);
		}

		return (long) -1;
	}

}
