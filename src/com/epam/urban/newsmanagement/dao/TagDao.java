package com.epam.urban.newsmanagement.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.epam.urban.newsmanagement.constant.Constants;
import com.epam.urban.newsmanagement.dao.pool.IConnectionPool;
import com.epam.urban.newsmanagement.exception.DAOException;

/**
 * Tag dao class
 * @author Urban Egor
 *
 */
public class TagDao extends BaseDao {

	private static final Logger LOG = Logger.getLogger(CommentDao.class);
		
	public TagDao(IConnectionPool connectionPool) {
		super(connectionPool);
		LOG.info("Tag dao has been initialized");
	}

	/**
	 * Add tag
	 * @param tag name
	 * @return tag id
	 * @throws DAOException
	 */
	public Long addTag(String tagName) throws DAOException {
		Connection connection = connectionPool.takeConnection();
		PreparedStatement statement = null;
		Long id = (long) -1;
		try {
			statement = openPrepStatement(connection, Constants.ADD_TAG);
			statement.setString(1, tagName);
			statement.executeUpdate();
			id = getTagId(tagName);
		} catch (SQLException e) {
			throw new DAOException("Adding tag failed", e);
		} finally {
			closeStatement(statement);
			connectionPool.releaseConnection(connection);
		}

		return id;
	}
	
	/**
	 * Add tag by news id
	 * @param news id
	 * @param tag id
	 * @return {@code true}, if tag successfully added
	 * @throws DAOException
	 */
	public boolean addTag(Long newsId, Long tagId) throws DAOException {
		boolean flag = false;
		Connection connection = connectionPool.takeConnection();
		PreparedStatement statement = null;

		try {
			statement = openPrepStatement(connection, Constants.ADD_NEWS_TAG);
			statement.setLong(1, newsId);
			statement.setLong(2, tagId);
			statement.executeUpdate();
			flag = true;
		} catch (SQLException e) {
			throw new DAOException("Adding tag failed", e);
		} finally {
			closeStatement(statement);
			connectionPool.releaseConnection(connection);
		}

		return flag;
	}
	
	/**
	 * To get tag id by tag name
	 * @param tag name
	 * @return tag id
	 * @throws DAOException
	 */
	public Long getTagId(String tagName) throws DAOException {
		Connection connection = connectionPool.takeConnection();
		PreparedStatement statement = null;

		try {
			statement = openPrepStatement(connection, Constants.GET_TAG_LIST);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				String name = resultSet.getString(Constants.TAG_NAME);
				if (tagName.equalsIgnoreCase(name)) {
					return resultSet.getLong(Constants.TAG_ID);
				}
			}
		} catch (SQLException e) {
			throw new DAOException("Getting tag id failed", e);
		} finally {
			closeStatement(statement);
			connectionPool.releaseConnection(connection);
		}

		return (long) -1;
	}
	
}
