package com.epam.urban.newsmanagement.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.epam.urban.newsmanagement.constant.Constants;
import com.epam.urban.newsmanagement.dao.pool.IConnectionPool;
import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.model.News;

/**
 * News dao class
 * @author Urban Egor
 *
 */
public class NewsDao extends BaseDao {

	private static final Logger LOG = Logger.getLogger(NewsDao.class);
	
	public NewsDao(IConnectionPool connectionPool) {
		super(connectionPool);
		LOG.info("News dao has been initialized");
	}

	/**
	 * Add news
	 * @param news
	 * @return {@code true}, if news successfully added
	 * @throws DAOException
	 */
	public boolean addNews(News news) throws DAOException {
		if (news == null) {
			throw new DAOException("News is null!");
		}
		boolean flag = false;
		Connection connection = connectionPool.takeConnection();
		PreparedStatement statement = null;

		try {
			statement = openPrepStatement(connection, Constants.ADD_NEWS);
			statement.setString(1, news.getShortText());
			statement.setString(2, news.getFullText());
			statement.setString(3, news.getTitle());
			statement.setString(4, news.getCreationDate().toString());
			statement.setString(5, news.getModificationDate().toString());
			statement.executeUpdate();
			flag = true;
		} catch (SQLException e) {
			throw new DAOException("Adding news failed", e);
		} finally {
			closeStatement(statement);
			connectionPool.releaseConnection(connection);
		}

		return flag;
	}

	/**
	 * Edit news
	 * @param news
	 * @return {@code true}, if news successfully changed
	 * @throws DAOException
	 */
	public boolean editNews(News news) throws DAOException {
		if (news == null) {
			throw new DAOException("News is null!");
		}
		boolean flag = false;
		Connection connection = connectionPool.takeConnection();
		PreparedStatement statement = null;

		try {
			statement = openPrepStatement(connection, Constants.EDIT_NEWS);
			statement.setString(1, news.getShortText());
			statement.setString(2, news.getFullText());
			statement.setString(3, news.getTitle());
			statement.setString(4, news.getCreationDate().toString());
			statement.setString(5, news.getModificationDate().toString());
			statement.setLong(6, news.getId());
			statement.executeUpdate();
			flag = true;
		} catch (SQLException e) {
			throw new DAOException("Edit news failed", e);
		} finally {
			closeStatement(statement);
			connectionPool.releaseConnection(connection);
		}

		return flag;
	}

	/**
	 * Delete news by id
	 * @param id
	 * @return {@code true}, if news successfully deleted
	 * @throws DAOException
	 */
	public boolean deleteNews(Long id) throws DAOException {
		boolean flag = false;
		Connection connection = connectionPool.takeConnection();
		PreparedStatement statement = null;
		PreparedStatement statement1 = null;
		PreparedStatement statement2 = null;
		PreparedStatement statement3 = null;

		try {
			connection.setAutoCommit(false);
			statement = openPrepStatement(connection, Constants.DEL_NEWS);
			statement1 = openPrepStatement(connection, Constants.DEL_NEWS_AUTHOR);
			statement2 = openPrepStatement(connection, Constants.DEL_COMMENTS);
			statement3 = openPrepStatement(connection, Constants.DEL_NEWS_TAG);
			statement.setLong(1, id);
			statement1.setLong(1, id);
			statement2.setLong(1, id);
			statement3.setLong(1, id);
			statement.executeUpdate();
			statement1.executeUpdate();
			statement2.executeUpdate();
			statement3.executeUpdate();
			connection.commit();
			flag = true;
		} catch (SQLException e) {
			try {
				connection.rollback();
				throw new DAOException("Deleting news failed", e);
			} catch (SQLException e1) {
				throw new DAOException("Rollback failed", e1);
			}
		} finally {
			closeStatement(statement);
			closeStatement(statement1);
			closeStatement(statement2);
			closeStatement(statement3);
			connectionPool.releaseConnection(connection);
		}

		return flag;
	}

	/**
	 * To get news list
	 * @return {@code List<News>} news list
	 * @throws DAOException
	 */
	public List<News> newsList() throws DAOException {
		List<News> list = new LinkedList<News>();
		Connection connection = connectionPool.takeConnection();
		PreparedStatement statement = null;

		try {
			statement = openPrepStatement(connection, Constants.LIST_NEWS);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				News news = new News();
				news.setId(resultSet.getLong(Constants.NEWS_ID));
				news.setShortText(resultSet.getString(Constants.SHORT_TEXT));
				news.setFullText(resultSet.getString(Constants.FULL_TEXT));
				news.setTitle(resultSet.getString(Constants.TITLE));
				news.setCreationDate(resultSet.getTimestamp(Constants.CREATION_DATE));
				news.setModificationDate(resultSet.getDate(Constants.MODIFICATION_DATE));
				list.add(news);
			}

		} catch (SQLException e) {
			throw new DAOException("Finding news list failed", e);
		} finally {
			closeStatement(statement);
			connectionPool.releaseConnection(connection);
		}

		return list;
	}

	/**
	 * To get news by id
	 * @param id
	 * @return {@code News} single news
	 * @throws DAOException
	 */
	public News getNewsById(Long id) throws DAOException {
		Connection connection = connectionPool.takeConnection();
		PreparedStatement statement = null;
		News news = null;
		try {
			statement = openPrepStatement(connection, Constants.GET_NEWS_BY_ID);
			statement.setLong(1, id);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				news = new News();
				news.setId(id);
				news.setShortText(resultSet.getString(Constants.SHORT_TEXT));
				news.setFullText(resultSet.getString(Constants.FULL_TEXT));
				news.setTitle(resultSet.getString(Constants.TITLE));
				news.setCreationDate(resultSet.getTimestamp(Constants.CREATION_DATE));
				news.setModificationDate(resultSet.getDate(Constants.MODIFICATION_DATE));
			}

		} catch (SQLException e) {
			throw new DAOException("Finding news by id failed", e);
		} finally {
			closeStatement(statement);
			connectionPool.releaseConnection(connection);
		}
		return news;
	}

	/**
	 * To get news list by author id
	 * @param author id
	 * @return {@code List<News>} list news by author
	 * @throws DAOException
	 */
	public List<News> getNewsByAuthor(Long authorId) throws DAOException {
		List<News> list = new ArrayList<News>();
		Connection connection = connectionPool.takeConnection();
		PreparedStatement statement = null;

		try {
			statement = openPrepStatement(connection, Constants.GET_NEWS_BY_AUTHOR);
			statement.setLong(1, authorId);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				News news = new News();
				news.setId(resultSet.getLong(Constants.NEWS_ID));
				news.setShortText(resultSet.getString(Constants.SHORT_TEXT));
				news.setFullText(resultSet.getString(Constants.FULL_TEXT));
				news.setTitle(resultSet.getString(Constants.TITLE));
				news.setCreationDate(resultSet.getTimestamp(Constants.CREATION_DATE));
				news.setModificationDate(resultSet.getDate(Constants.MODIFICATION_DATE));
				list.add(news);
			}

		} catch (SQLException e) {
			throw new DAOException("Finding news list by author failed", e);
		} finally {
			closeStatement(statement);
			connectionPool.releaseConnection(connection);
		}

		return list;
	}

	/**
	 * To get news list by tag id
	 * @param tag id
	 * @return {@code List<News>} list news by tag
	 * @throws DAOException
	 */
	public List<News> getNewsByTag(Long tagId) throws DAOException {
		List<News> list = new ArrayList<News>();
		Connection connection = connectionPool.takeConnection();
		PreparedStatement statement = null;

		try {
			statement = openPrepStatement(connection, Constants.GET_NEWS_BY_TAG);
			statement.setLong(1, tagId);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				News news = new News();
				news.setId(resultSet.getLong(Constants.NEWS_ID));
				news.setShortText(resultSet.getString(Constants.SHORT_TEXT));
				news.setFullText(resultSet.getString(Constants.FULL_TEXT));
				news.setTitle(resultSet.getString(Constants.TITLE));
				news.setCreationDate(resultSet.getTimestamp(Constants.CREATION_DATE));
				news.setModificationDate(resultSet.getDate(Constants.MODIFICATION_DATE));
				list.add(news);
			}

		} catch (SQLException e) {
			throw new DAOException("Finding news list by tag failed", e);
		} finally {
			closeStatement(statement);
			connectionPool.releaseConnection(connection);
		}

		return list;
	}

	/**
	 * To get count of one news comments
	 * @param news id
	 * @return {@code int} Count
	 * @throws DAOException
	 */
	public int getCountOfComments(Long newsId) throws DAOException {
		Connection connection = connectionPool.takeConnection();
		PreparedStatement statement = null;

		try {
			statement = openPrepStatement(connection, Constants.GET_COUNT);
			statement.setLong(1, newsId);
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) {
				return resultSet.getInt(1);
			}
		} catch (SQLException e) {
			throw new DAOException("Getting count failed", e);
		} finally {
			closeStatement(statement);
			connectionPool.releaseConnection(connection);
		}

		return -1;

	}

}
