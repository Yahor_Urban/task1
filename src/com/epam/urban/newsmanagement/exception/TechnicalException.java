package com.epam.urban.newsmanagement.exception;

public class TechnicalException extends Exception {
	
	private static final long serialVersionUID = -1936506238381868996L;

	public TechnicalException() {
	}
	
	public TechnicalException(String message, Throwable exception) {
		super(message, exception);
	}
	
	public TechnicalException(String message) {
		super(message);
	}
	
	public TechnicalException(Throwable exception) {
		super(exception);
	}

}
