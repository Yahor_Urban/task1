package com.epam.urban.newsmanagement.exception;

public class LogicException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LogicException() {
	}
	
	public LogicException(String message, Throwable exception) {
		super(message, exception);
	}
	
	public LogicException(String message) {
		super(message);
	}
	
	public LogicException(Throwable exception) {
		super(exception);
	}

}
