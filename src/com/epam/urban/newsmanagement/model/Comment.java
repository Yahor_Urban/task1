package com.epam.urban.newsmanagement.model;

import java.io.Serializable;
import java.sql.Timestamp;

public class Comment extends Entity implements Serializable {

	private static final long serialVersionUID = -3221917448586192717L;

	private String commentText;
	private Timestamp creationDate;
	private Long newsId;

	public Comment() {
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

}
