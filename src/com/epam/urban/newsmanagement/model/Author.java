package com.epam.urban.newsmanagement.model;

import java.io.Serializable;

public class Author extends Entity implements Serializable {

	private static final long serialVersionUID = -3298514992549935385L;

	private Long authorId;
	private Long newsId;
	private String name;

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}
}
