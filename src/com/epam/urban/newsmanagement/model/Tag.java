package com.epam.urban.newsmanagement.model;

import java.io.Serializable;

public class Tag extends Entity implements Serializable {

	private static final long serialVersionUID = -5040724917595944896L;

	private Long tagId;
	private Long newsId;
	private String tagName;

	public Long getTagId() {
		return tagId;
	}

	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

}
