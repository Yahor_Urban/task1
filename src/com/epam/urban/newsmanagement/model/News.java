package com.epam.urban.newsmanagement.model;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;


public class News extends Entity implements Serializable {

	private static final long serialVersionUID = -1235441502844629004L;

	private String shortText;
	private String fullText;
	private String title;
	private Timestamp creationDate;
	private Date modificationDate;

	public News() {
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

}
