package com.epam.urban.newsmanagement.logic.service;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.epam.urban.newsmanagement.dao.DaoFactory;
import com.epam.urban.newsmanagement.dao.IDaoFactory;
import com.epam.urban.newsmanagement.dao.TagDao;
import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.exception.LogicException;
import com.epam.urban.newsmanagement.model.Tag;

/**
 * tag service class
 * @author Urban Egor
 *
 */
public class TagService implements IService<Tag>{

	private static final Lock LOCK = new ReentrantLock();
	private static volatile TagService instance;
	private TagDao tagDao;
		
	private TagService() throws LogicException {
		IDaoFactory daoFactory = null;
		try {
			daoFactory = DaoFactory.getInstance();
		} catch (DAOException e) {
			throw new LogicException("Dao initialization failed");
		}
		this.tagDao = daoFactory.getTagDao();
	}

	/**
	 * Initializes service
	 * @return {@code TagService} instance
	 * @throws LogicException
	 */
	public static TagService getInstance() throws LogicException {
		if (instance == null) {
			try {
				LOCK.lock();
				if (instance == null) {
					instance = new TagService();
				}
			} finally {
				LOCK.unlock();
			}
		}
		return instance;
	}
	
	/**
	 * For mockito test
	 * @param dao
	 */
	public TagService(TagDao dao) {
		this.tagDao = dao;
	}
	
	/**
	 * Add tag
	 * @param tag name
	 * @return {@code true}, if successfully added
	 * @throws LogicException
	 */
	public boolean add(String tagName) throws LogicException {
		if (tagName == null || tagName.isEmpty()) {
			throw new LogicException("Tag name is empty");
		}
		boolean flag = false;
		try {
			Long tagId = tagDao.getTagId(tagName);
			if (tagId == -1) {
				tagDao.addTag(tagName);
				flag = true;
			} else {
				throw new LogicException("Tag is exist");
			}
		} catch (DAOException e) {
			throw new LogicException("Adding tag failed", e);
		}
		
		return flag;
	}
	
	@Override
	public boolean add(Tag tag) throws LogicException {
		if (tag.getTagName() == null || tag.getTagName().isEmpty()) {
			throw new LogicException("Tag name is empty");
		} else if (tag.getNewsId() == null) {
			throw new LogicException("Incorrect newsId");
		}
		boolean flag = false;
		try {
			Long authorId = tagDao.getTagId(tag.getTagName());
			if (authorId == -1) {
				authorId = tagDao.addTag(tag.getTagName());
			}
			if (!tagDao.addTag(tag.getNewsId(), tag.getTagId())) {
				throw new LogicException("Adding tag failed");
			}
			flag = true;
		} catch (DAOException e) {
			throw new LogicException("Adding tag failed", e);
		}
		
		return flag;
	}

	@Override
	public boolean edit(Tag entity) throws LogicException {
		return false;
	}

	@Override
	public boolean delete(Long id) throws LogicException {
		return false;
	}
}
