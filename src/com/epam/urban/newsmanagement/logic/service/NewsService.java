package com.epam.urban.newsmanagement.logic.service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.epam.urban.newsmanagement.dao.AuthorDao;
import com.epam.urban.newsmanagement.dao.DaoFactory;
import com.epam.urban.newsmanagement.dao.IDaoFactory;
import com.epam.urban.newsmanagement.dao.NewsDao;
import com.epam.urban.newsmanagement.dao.TagDao;
import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.exception.LogicException;
import com.epam.urban.newsmanagement.model.Comment;
import com.epam.urban.newsmanagement.model.News;

/**
 * News service class
 * @author Urban Egor
 *
 */
public class NewsService implements IService<News>{

	private static final Lock LOCK = new ReentrantLock();
	private static volatile NewsService instance;
	private NewsDao newsDao;
	private AuthorDao authorDao;
	private TagDao tagDao;

	private NewsService() throws LogicException {
		IDaoFactory daoFactory = null;
		try {
			daoFactory = DaoFactory.getInstance();
		} catch (DAOException e) {
			throw new LogicException("Dao initialization failed");
		}
		this.newsDao = daoFactory.getNewsDao();
		this.authorDao = daoFactory.getAuthorDao();
		this.tagDao = daoFactory.getTagDao();

	}

	/**
	 * Initializes service
	 * @return {@code NewsService} instance
	 * @throws LogicException
	 */
	public static NewsService getInstance() throws LogicException {
		if (instance == null) {
			try {
				LOCK.lock();
				if (instance == null) {
					instance = new NewsService();
				}
			} finally {
				LOCK.unlock();
			}
		}
		return instance;
	}

	/**
	 * For mockito test
	 * @param dao
	 */
	public NewsService(NewsDao dao) {
		this.newsDao = dao;
	}

	@Override
	public boolean add(News news) throws LogicException {
		if (news == null) {
			throw new LogicException("News is null");
		}

		try {
			if (!newsDao.addNews(news)) {
				return false;
			}
		} catch (DAOException e) {
			throw new LogicException("Adding news failed", e);
		}

		return true;
	}

	@Override
	public boolean edit(News news) throws LogicException {
		if (news == null) {
			throw new LogicException("News is null");
		}

		try {
			if (!newsDao.editNews(news)) {
				return false;
			}
		} catch (DAOException e) {
			throw new LogicException("Edit news failed", e);
		}

		return true;
	}

	@Override
	public boolean delete(Long newsId) throws LogicException {
		boolean flag = false;
		if (newsId < 0 || newsId == null) {
			throw new LogicException("Incorrect newsId");
		}

		try {
			if (!newsDao.deleteNews(newsId)) {
				throw new LogicException("Deleting news failed");
			}
			flag = true;
		} catch (DAOException e) {
			throw new LogicException("Deleting news failed", e);
		}
		return flag;
	}

	/**
	 * To get news list
	 * @return {@code List<News>} news list
	 * @throws LogicException
	 */
	public List<News> getNewsList() throws LogicException {
		List<News> list = null;
		try {
			list = newsDao.newsList();
			if (list == null) {
				throw new LogicException("News list is null");
			}
		} catch (DAOException e) {
			throw new LogicException("Getting news list failed", e);
		}

		return list;
	}

	/**
	 * To get news by id
	 * @param id
	 * @return {@code News} single news
	 * @throws LogicException
	 */
	public News getSingleNewsMessage(Long newsId) throws LogicException {
		if (newsId < 0 || newsId == null) {
			throw new LogicException("Incorrect newsId");
		}
		News news = null;
		try {
			news = newsDao.getNewsById(newsId);
			if (news == null) {
				throw new LogicException("Single news message is null");
			}
		} catch (DAOException e) {
			throw new LogicException("Getting news list failed", e);
		}

		return news;
	}

	/**
	 * To get news list by author id
	 * @param author name
	 * @return {@code List<News>} list news by author
	 * @throws LogicException
	 */
	public List<News> getNewsByAuthor(String authorName) throws LogicException {
		if (authorName == null || authorName.isEmpty()) {
			throw new LogicException("Author name is empty");
		}
		List<News> list = new ArrayList<News>();

		try {
			Long authorId = authorDao.getAuthorId(authorName);
			if (authorId == -1) {
				throw new LogicException("Author is not exist");
			} else {
				list = newsDao.getNewsByAuthor(authorId);
			}
		} catch (DAOException e) {
			throw new LogicException("Gettin news by author failed", e);
		}

		return list;
	}

	/**
	 * To get news list by tag id
	 * @param tag name
	 * @return {@code List<News>} list news by tag
	 * @throws LogicException
	 */
	public List<News> getNewsByTag(String tagName) throws LogicException {
		if (tagName == null || tagName.isEmpty()) {
			throw new LogicException("Tag name is empty");
		}
		List<News> list = new ArrayList<News>();

		try {
			Long tagId = tagDao.getTagId(tagName);
			if (tagId == -1) {
				throw new LogicException("Tag is not exist");
			} else {
				list = newsDao.getNewsByTag(tagId);
			}
		} catch (DAOException e) {
			throw new LogicException("Gettin news by tag failed", e);
		}

		return list;
	}

	/**
	 * To get sorted by count of comments news list
	 * @return {@code TreeMap<Integer, News>} list of sorted news
	 * @throws LogicException
	 */
	public TreeMap<Integer, News> getSortedNewsList() throws LogicException {
		List<News> news = new LinkedList<News>();
		TreeMap<Integer, News> treeMap = new TreeMap<Integer, News>();

		try {
			news = newsDao.newsList();
		} catch (DAOException e) {
			throw new LogicException("Getting news list failed", e);
		}

		try {
			for (News n : news) {
				int count = newsDao.getCountOfComments(n.getId());
				treeMap.put(count, n);
			}
		} catch (DAOException e) {
			throw new LogicException("Getting count of comments failed", e);
		}

		return treeMap;
	}

	/**
	 * Add news with comment and tags
	 * @param news
	 * @param comment
	 * @param tags list
	 * @throws LogicException
	 */
	public void add(News news, Comment comment, List<String> tags) throws LogicException {
		if (news == null) {
			throw new LogicException("Incorrect data!");
		}
		CommentService commentServise = CommentService.getInstance();
		TagService tagService = TagService.getInstance();

		add(news);
		if (!comment.getCommentText().isEmpty()) {
			commentServise.add(comment);
		}
		if (tags.size() != 0) {
			for (String tagName : tags) {
				tagService.add(tagName);
			}
		}
	}
}
