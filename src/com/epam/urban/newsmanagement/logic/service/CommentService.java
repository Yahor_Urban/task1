package com.epam.urban.newsmanagement.logic.service;

import java.sql.Timestamp;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.epam.urban.newsmanagement.dao.CommentDao;
import com.epam.urban.newsmanagement.dao.DaoFactory;
import com.epam.urban.newsmanagement.dao.IDaoFactory;
import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.exception.LogicException;
import com.epam.urban.newsmanagement.model.Comment;

public class CommentService implements IService<Comment>{

	private static final Lock LOCK = new ReentrantLock();
	private static volatile CommentService instance;
	private CommentDao commentDao;

	private CommentService() throws LogicException {
		IDaoFactory daoFactory = null;
		try {
			daoFactory = DaoFactory.getInstance();
		} catch (DAOException e) {
			throw new LogicException("Dao initialization failed");
		}
		this.commentDao = daoFactory.getCommentDao();

	}

	/**
	 * Initializes service
	 * @return {@code CommentService} instance
	 * @throws LogicException
	 */
	public static CommentService getInstance() throws LogicException {
		if (instance == null) {
			try {
				LOCK.lock();
				if (instance == null) {
					instance = new CommentService();
				}
			} finally {
				LOCK.unlock();
			}
		}
		return instance;
	}
	
	@Override
	public boolean add(Comment comment) throws LogicException {
		if (comment.getCommentText() == null || comment.getCommentText().isEmpty()) {
			throw new LogicException("Comment text is empty");
		} else if (comment.getNewsId() == null) {
			throw new LogicException("Incorrect newsId");
		}
		boolean flag = false;
		comment.setCreationDate(new Timestamp(System.currentTimeMillis()));
		
		try {
			commentDao.addComment(comment);
			flag = true;
		} catch (DAOException e) {
			throw new LogicException("Adding comment failed", e);
		}
		
		return flag;
	}
	
	@Override
	public boolean delete(Long commentId) throws LogicException {
		if (commentId < 0 || commentId == null) {
			throw new LogicException("Incorrect commentId");
		}
		boolean flag = false;
		try {
			if (!commentDao.deleteComment(commentId)) {
				throw new LogicException("Deleting comment failed");
			}
			flag = true;
		} catch (DAOException e) {
			throw new LogicException("Deleting comment failed", e);
		}
		
		return flag;
	}

	@Override
	public boolean edit(Comment comment) throws LogicException {
		if (comment.getCommentText() == null || comment.getCommentText().isEmpty()) {
			throw new LogicException("Comment text is empty");
		} else if (comment.getId() == null) {
			throw new LogicException("Incorrect commentId");
		}
		boolean flag = false;
		try {
			commentDao.editComment(comment);
			flag = true;
		} catch (DAOException e) {
			throw new LogicException("Edit comment failed");
		}
		
		return flag;
	}
}
