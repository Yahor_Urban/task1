package com.epam.urban.newsmanagement.logic.service;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.epam.urban.newsmanagement.dao.AuthorDao;
import com.epam.urban.newsmanagement.dao.DaoFactory;
import com.epam.urban.newsmanagement.dao.IDaoFactory;
import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.exception.LogicException;
import com.epam.urban.newsmanagement.model.Author;

/**
 * Author service class
 * 
 * @author Urban Egor
 * 
 */
public class AuthorService implements IService<Author> {

	private static final Lock LOCK = new ReentrantLock();
	private static volatile AuthorService instance;
	private AuthorDao authorDao;

	private AuthorService() throws LogicException {
		IDaoFactory daoFactory = null;
		try {
			daoFactory = DaoFactory.getInstance();
		} catch (DAOException e) {
			throw new LogicException("Dao initialization failed");
		}
		this.authorDao = daoFactory.getAuthorDao();

	}

	/**
	 * Initializes service
	 * 
	 * @return {@code AuthorService} instance
	 * @throws LogicException
	 */
	public static AuthorService getInstance() throws LogicException {
		if (instance == null) {
			try {
				LOCK.lock();
				if (instance == null) {
					instance = new AuthorService();
				}
			} finally {
				LOCK.unlock();
			}
		}
		return instance;
	}

	/**
	 * For mockito test
	 * 
	 * @param dao
	 */
	public AuthorService(AuthorDao dao) {
		this.authorDao = dao;
	}

	@Override
	public boolean add(Author author) throws LogicException {
		if (author.getName() == null || author.getName().isEmpty()) {
			throw new LogicException("Author name is empty");
		} else if (author.getNewsId() == null) {
			throw new LogicException("Incorrect newsId");
		}
		boolean flag = false;
		try {
			Long authorId = authorDao.getAuthorId(author.getName());
			if (authorId == -1) {
				authorId = authorDao.addAuthor(author.getName());
			}
			if (!authorDao.addAuthor(author.getNewsId(), authorId)) {
				throw new LogicException("Adding news author failed");
			}
			flag = true;
		} catch (DAOException e) {
			throw new LogicException("Adding news author failed", e);
		}

		return flag;
	}

	/**
	 * Add author
	 * 
	 * @param name
	 * @return {@code true}, if successfully added
	 * @throws LogicException
	 */
	public boolean addAuthor(String name) throws LogicException {
		if (name == null || name.isEmpty()) {
			throw new LogicException("Author name is empty");
		}
		boolean flag = false;
		try {
			Long authorId = authorDao.getAuthorId(name);
			if (authorId == -1) {
				authorDao.addAuthor(name);
				flag = true;
			} else {
				throw new LogicException("News author is exist");
			}
		} catch (DAOException e) {
			throw new LogicException("Adding news author failed", e);
		}
		return flag;
	}

	@Override
	public boolean edit(Author author) throws LogicException {
		if (author.getAuthorId() == null || author.getName() == null || author.getName().isEmpty()) {
			throw new LogicException("Incorrect data");
		}
		boolean flag = false;
		
		try {
			authorDao.editAuthor(author);
			flag = true;
		} catch (DAOException e) {
			throw new LogicException("Editing news author failed", e);
		}
		
		return flag;
	}

	@Override
	public boolean delete(Long id) throws LogicException {
		if (id == null) {
			throw new LogicException("Author id is null");
		}
		boolean flag = false;
		
		try {
			authorDao.deleteAuthor(id);
			flag = true;
		} catch (DAOException e) {
			throw new LogicException("Deleting news author failed", e);
		}
				
		return flag;
	}
}
