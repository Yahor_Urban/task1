package com.epam.urban.newsmanagement.logic.service;

import com.epam.urban.newsmanagement.exception.LogicException;
import com.epam.urban.newsmanagement.model.Entity;

/**
 * Service interface
 * @author Urban Egor
 *
 * @param <T> data transfer object
 */
public interface IService<T extends Entity> {

	/**
	 * 
	 * @param data transfer object
	 * @return {@code true}, if dto successfully added
	 * @throws LogicException
	 */
	public boolean add(T entity) throws LogicException;

	/**
	 * 
	 * @param data transfer object
	 * @return {@code true}, if successfully changed
	 * @throws LogicException
	 */
	public boolean edit(T entity) throws LogicException;

	/**
	 * 
	 * @param id
	 * @return {@code true}, if successfully deleted
	 * @throws LogicException
	 */
	public boolean delete(Long id) throws LogicException;

}
