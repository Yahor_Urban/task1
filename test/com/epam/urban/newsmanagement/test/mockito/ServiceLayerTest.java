package com.epam.urban.newsmanagement.test.mockito;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;

import org.junit.Test;

import com.epam.urban.newsmanagement.dao.AuthorDao;
import com.epam.urban.newsmanagement.dao.NewsDao;
import com.epam.urban.newsmanagement.dao.TagDao;
import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.exception.LogicException;
import com.epam.urban.newsmanagement.logic.service.AuthorService;
import com.epam.urban.newsmanagement.logic.service.NewsService;
import com.epam.urban.newsmanagement.logic.service.TagService;
import com.epam.urban.newsmanagement.model.Author;
import com.epam.urban.newsmanagement.model.News;
import com.epam.urban.newsmanagement.model.Tag;


public class ServiceLayerTest {

	@Test
	public void testNewsService() throws DAOException, LogicException {
		News news = new News();
		News news1 = new News();
		news.setFullText("fulltext");
		news.setShortText("shortText");
		news.setId((long) 1);
		news1.setId((long) 2);
		
		TreeMap<Integer, News> treeMap = new TreeMap<Integer, News>();
		treeMap.put(5, news);
		treeMap.put(7, news1);
		
		List<News> newsList = new LinkedList<News>();
		newsList.add(news1);
		newsList.add(news);
		
		
		NewsDao newsDaoMock = mock(NewsDao.class);
		NewsService newsServise = new NewsService(newsDaoMock);
		
		when(newsDaoMock.addNews(news)).thenReturn(true);
		when(newsDaoMock.editNews(news)).thenReturn(true);
		when(newsDaoMock.deleteNews(news.getId())).thenReturn(true);
		when(newsDaoMock.newsList()).thenReturn(newsList);
		when(newsDaoMock.getCountOfComments((long) 1)).thenReturn(5);
		when(newsDaoMock.getCountOfComments((long) 2)).thenReturn(7);
		
		assertEquals(true, newsServise.add(news));
		assertEquals(true, newsServise.edit(news));
		assertEquals(true, newsServise.delete(news.getId()));
		assertEquals(treeMap, newsServise.getSortedNewsList());
	}
	
	@Test
	public void testAuthorServise() throws DAOException, LogicException {
		Author author = new Author();
		author.setAuthorId((long) 1);
		author.setName("name");
		author.setNewsId((long) 1);
		
		AuthorDao authorDaoMock = mock(AuthorDao.class);
		AuthorService authorServise = new AuthorService(authorDaoMock);
		when(authorDaoMock.getAuthorId(author.getName())).thenReturn((long) -1);
		when(authorDaoMock.addAuthor(author.getName())).thenReturn((long) 1);
		when(authorDaoMock.addAuthor(author.getNewsId(), author.getAuthorId())).thenReturn(true);
				
		assertEquals(true, authorServise.add(author));
		assertEquals(true, authorServise.addAuthor(author.getName()));
	}
	
	@Test
	public void testTagServise() throws DAOException, LogicException {
		Tag tag = new Tag();
		tag.setTagId((long) 1);
		tag.setTagName("name");
		tag.setNewsId((long) 1);
		
		TagDao tagDaoMock = mock(TagDao.class);
		TagService tagServise = new TagService(tagDaoMock);
		when(tagDaoMock.getTagId(tag.getTagName())).thenReturn((long) -1);
		when(tagDaoMock.addTag(tag.getTagName())).thenReturn((long) 1);
		when(tagDaoMock.addTag(tag.getNewsId(), tag.getTagId())).thenReturn(true);
				
		assertEquals(true, tagServise.add(tag.getTagName()));
		assertEquals(true, tagServise.add(tag));
	}
}
