package com.epam.urban.newsmanagement.test;

import static org.junit.Assert.assertNotNull;

import java.sql.Connection;

import org.junit.AfterClass;
import org.junit.Test;

import com.epam.urban.newsmanagement.dao.pool.ConnectionPool;
import com.epam.urban.newsmanagement.dao.pool.IConnectionPool;
import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.resource.DatabaseResources;
import com.epam.urban.newsmanagement.resource.IResources;


public class poolTest {

	private static IConnectionPool connectionPool;
	private IResources resources;
	
	@Test
	public void initializingPool() throws DAOException {
		resources = DatabaseResources.getInstance();
		connectionPool = ConnectionPool.getInstance();
		assertNotNull("test ", connectionPool);
		int poolSize = Integer.parseInt(resources.getString("connection.poolsize"));
		
		for (int i = 0; i < poolSize; i++) {
			Connection connection = connectionPool.takeConnection();
			assertNotNull("test ", connection);
		}
	}

	@AfterClass
	public static void disposePool() {
		connectionPool.dispose();
	}

}
