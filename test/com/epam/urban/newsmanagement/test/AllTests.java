package com.epam.urban.newsmanagement.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.epam.urban.newsmanagement.test.dbunit.DAOTest;
import com.epam.urban.newsmanagement.test.mockito.ServiceLayerTest;

@RunWith(Suite.class)
@SuiteClasses({ poolTest.class, DAOTest.class, ServiceLayerTest.class })
public class AllTests {

}
