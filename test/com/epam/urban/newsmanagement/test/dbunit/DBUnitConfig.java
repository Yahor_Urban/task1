package com.epam.urban.newsmanagement.test.dbunit;

import org.dbunit.DBTestCase;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.dataset.IDataSet;
import org.dbunit.ext.oracle.OracleDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;

import com.epam.urban.newsmanagement.constant.Constants;
import com.epam.urban.newsmanagement.dao.AuthorDao;
import com.epam.urban.newsmanagement.dao.CommentDao;
import com.epam.urban.newsmanagement.dao.DaoFactory;
import com.epam.urban.newsmanagement.dao.IDaoFactory;
import com.epam.urban.newsmanagement.dao.NewsDao;
import com.epam.urban.newsmanagement.dao.TagDao;
import com.epam.urban.newsmanagement.resource.DatabaseResources;
import com.epam.urban.newsmanagement.resource.IResources;

public class DBUnitConfig extends DBTestCase {

	protected IDatabaseTester tester;
	private IResources resources;
	protected IDataSet beforeData;
	protected NewsDao newsDao;
	protected CommentDao commentDao;
	protected TagDao tagDao;
	protected AuthorDao authorDao;
	protected IDataSet expectedDataSet;

	@Override
	protected void setUp() throws Exception {
		tester = new JdbcDatabaseTester(resources.getString(Constants.DRIVER), resources.getString(Constants.URL),
				resources.getString(Constants.USER), resources.getString(Constants.PASSWORD));
		IDaoFactory daoFactory = DaoFactory.getInstance();
		newsDao = daoFactory.getNewsDao();
		commentDao = daoFactory.getCommentDao();
		tagDao = daoFactory.getTagDao();
		authorDao = daoFactory.getAuthorDao();
	}

	public DBUnitConfig(String name) {
		super(name);
		resources = DatabaseResources.getInstance();

		System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS, resources.getString(Constants.DRIVER));
		System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL, resources.getString(Constants.URL));
		System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME, resources.getString(Constants.USER));
		System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD, resources.getString(Constants.PASSWORD));
		System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_SCHEMA, "ROOT");
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return beforeData;
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE_ALL;
	}

	@Override
	protected void setUpDatabaseConfig(DatabaseConfig config) {
		config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new OracleDataTypeFactory());
	}
	
}
