package com.epam.urban.newsmanagement.test.dbunit;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.dbunit.Assertion;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.filter.DefaultColumnFilter;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Before;
import org.junit.Test;

import com.epam.urban.newsmanagement.model.Author;
import com.epam.urban.newsmanagement.model.Comment;
import com.epam.urban.newsmanagement.model.News;

public class DAOTest extends DBUnitConfig {

	@Before
	public void setUp() throws Exception {
		super.setUp();
		beforeData = new FlatXmlDataSetBuilder().build(Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("data/TestDataSet.xml"));
		expectedDataSet = new FlatXmlDataSetBuilder().build(Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("data/TestDataSetExpected.xml"));
		tester.setDataSet(beforeData);
		tester.onSetup();
	}

	@Test
	public void testNewsDao() throws SQLException, Exception {
		News news = new News();
		News newsExpected = null;
		List<News> newsList = null;
		news.setId((long) 3);
		news.setShortText("short3");
		news.setFullText("fulltext3");
		news.setTitle("title3");
		news.setCreationDate(new Timestamp(System.currentTimeMillis()));
		java.util.Date date = new java.util.Date();
		long t = date.getTime();
		java.sql.Date sqlDate = new java.sql.Date(t);
		news.setModificationDate(sqlDate);
		newsDao.addNews(news);

		news.setId((long) 2);
		news.setShortText("shortnew");
		news.setFullText("fulltextnew");
		news.setTitle("titlenew");
		newsDao.editNews(news);
		
		newsList = newsDao.newsList();
		newsExpected = newsDao.getNewsById((long) 1);
		newsList = newsDao.getNewsByTag((long) 1);
		int c = newsDao.getCountOfComments((long) 1);
		
		ITable expectedTableNews = expectedDataSet.getTable("NEWS");
		ITable filteredActualTable = DefaultColumnFilter.includedColumnsTable(expectedTableNews, expectedTableNews
				.getTableMetaData().getColumns());
	
		Assertion.assertEquals(expectedTableNews, filteredActualTable);
		assertNotNull("newsList is null ", newsList);
		assertEquals("short1", newsExpected.getShortText());
		assertEquals(2, c);
	}

	@Test
	public void testAuthorDao() throws Exception {
		Author author = new Author();
		author.setAuthorId((long) 1);
		author.setName("Sasha");
		authorDao.addAuthor("Mariya");
		authorDao.addAuthor((long) 3,(long) 3);
		authorDao.editAuthor(author);
		authorDao.deleteAuthor((long) 2);
		
		ITable expectedTableAuthor = expectedDataSet.getTable("AUTHOR");
		ITable filteredActualTableAuthor = DefaultColumnFilter.includedColumnsTable(expectedTableAuthor,
				expectedTableAuthor.getTableMetaData().getColumns());
		ITable expectedTableAuthorId = expectedDataSet.getTable("NEWS_AUTHOR");
		ITable filteredActualTableAuthorId = DefaultColumnFilter.includedColumnsTable(expectedTableAuthorId,
				expectedTableAuthorId.getTableMetaData().getColumns());
		
		Assertion.assertEquals(expectedTableAuthor, filteredActualTableAuthor);
		Assertion.assertEquals(expectedTableAuthorId, filteredActualTableAuthorId);
	}
	
	@Test
	public void testCommentDao() throws Exception {
		Comment comment = new Comment();
		comment.setCommentText("comment3");
		comment.setCreationDate(new Timestamp(System.currentTimeMillis()));
		comment.setNewsId((long) 2);
		commentDao.addComment(comment);
		comment.setId((long) 1);
		comment.setCommentText("new_comment");
		commentDao.editComment(comment);
		
		ITable expectedTableComment = expectedDataSet.getTable("COMMENTS");
		ITable filteredActualTableComment = DefaultColumnFilter.includedColumnsTable(expectedTableComment,
				expectedTableComment.getTableMetaData().getColumns());
		
		Assertion.assertEquals(expectedTableComment, filteredActualTableComment);
	}
	
	@Test
	public void testTagDao() throws Exception {
		tagDao.addTag("tag3");
		tagDao.addTag((long) 3, (long) 3);
		
		ITable expectedTableTag = expectedDataSet.getTable("TAG");
		ITable filteredActualTableTag = DefaultColumnFilter.includedColumnsTable(expectedTableTag,
				expectedTableTag.getTableMetaData().getColumns());
		ITable expectedTableNewsTag = expectedDataSet.getTable("NEWS_TAG");
		ITable filteredActualTableNewsTag = DefaultColumnFilter.includedColumnsTable(expectedTableNewsTag,
				expectedTableNewsTag.getTableMetaData().getColumns());
		
		Assertion.assertEquals(expectedTableTag, filteredActualTableTag);
		Assertion.assertEquals(expectedTableNewsTag, filteredActualTableNewsTag);
	}
	
	public DAOTest(String name) {
		super(name);
	}

}
